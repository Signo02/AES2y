clear all; close all

% punto 1 function diff_finite.m

% punto 2

f = @(x) sin(2*x).*cos(x)+exp(x/2);
Df = @(x) 2*cos(2*x).*cos(x) - sin(2*x).*sin(x) + 0.5*exp(x/2);

a = 0;
b = 4;

xgrid = linspace(a,b,1000);

plot(xgrid,f(xgrid),'-',xgrid,Df(xgrid),'--')
legend('f(x)','f''(x)','Location','NorthWest')


% punto 3
h = 0.2;
x = a:h:b;

deltaf = diff_finite(f,x,h,[0,1/2,0,-1/2,0]);

figure;
plot(x,deltaf,'*-',xgrid,Df(xgrid),'-');
title('Differenze centrate')

% punto 4
hs = 0.1*2.^(-(0:5));
err = [];
for h = hs
    deltaf = diff_finite(f,x,h,[0,1/2,0,-1/2,0]);
    err = [err,max(abs(deltaf-Df(x)))];
end
figure
loglog(hs,err,'-',hs,hs.^2,'--')
title('Errore differenze centrate')
legend('Errore','h^2')

% punto 5
AA=[1 1 1 1 1;
    2 1 0 -1 -2;
    2 0.5 0 0.5 2;
    4/3 1/6 0 -1/6 -4/3;
    2/3 1/24 0 1/24 2/3];

bb = [0;1;0;0;0];

h = 0.2;
x = a:h:b;
coeff = AA\bb;

deltaf = diff_finite(f,x,h,coeff);

figure;
plot(x,deltaf,'*-',xgrid,Df(xgrid),'-');
title('Formula di ordine 4')


hs = 0.1*2.^(-(0:5));
err = [];
for h = hs
    deltaf = diff_finite(f,x,h,coeff);
    err = [err,max(abs(deltaf-Df(x)))];
end
figure
loglog(hs,err,'-',hs,hs.^4,'--')
title('Errore formula di ordine 4')
legend('Errore','h^4')