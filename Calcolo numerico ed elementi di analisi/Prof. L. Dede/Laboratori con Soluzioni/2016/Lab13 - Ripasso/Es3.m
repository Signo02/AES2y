addpath ../Fornite
%%%% Es 3 %%%%
close all
clear all
% definizione della matrice
n=10;
B=15*diag(ones(n,1))+2*diag(ones(n-1,1),-1)+2*diag(ones(n-1,1),+1)+...
  5*diag(ones(n-2,1),-2)+5*diag(ones(n-2,1),+2);

% definizione della soluzione esatta e calcolo del termine noto
x_ex=2*ones(n,1);
d=B*x_ex;

% definizione delle matrici di iterazione di Jacobi e Gauss-Seidel
BJ=eye(n)-diag(1./diag(B))*B;
BGS=eye(n)-tril(B)\B;

% calcolo dei raggi spettrali delle matrici di iterazione
Lambda_J=eig(BJ);
Lambda_GS=eig(BGS);
fprintf('\n');
fprintf(['Raggio spettrale Jacobi : ',num2str(max(abs(Lambda_J))),'\n']);
fprintf(['Raggio spettrale Jacobi : ',num2str(max(abs(Lambda_GS))),'\n']);

% calcolo della soluzione del sistema Bx=d tramite il metodo di Richardson,
% per diversi valori del parametro di accelerazione alpha
x0=zeros(n,1);
LL=eig(B);
alpha_opt=2/(min(LL)+max(LL));
maxit=100;
toll=1.e-6;
fprintf('\n\t_____ alpha=alpha_opt _____\n\n');
[x,it]=Richardson(B,d,x0,alpha_opt,maxit,toll);
fprintf([' iterazioni = ',num2str(it),'\n']);
fprintf([' errore relativo commesso = ',num2str(norm(x-x_ex)/norm(x_ex)),...
        '\n']);
fprintf('\n\t_____ alpha=0.03 _____\n\n');
[x,it]=Richardson(B,d,x0,0.03,maxit,toll);
fprintf([' iterazioni = ',num2str(it),'\n']);
fprintf([' errore relativo commesso = ',num2str(norm(x-x_ex)/norm(x_ex)),...
        '\n']);
fprintf('\n\t_____ alpha=0.1 _____\n\n');
[x,it]=Richardson(B,d,x0,0.1,maxit,toll);
fprintf([' iterazioni = ',num2str(it),'\n']);
fprintf([' errore relativo commesso = ',num2str(norm(x-x_ex)/norm(x_ex)),...
        '\n']);