A = eye(10)
spy(A) %% Sparsity pattern
v = eig(A) %eigenvalues
[v,D] = eig(A) %eigenvalues + eigenvectorsMatrix

%% 1.1)
n = 100;
A = -2*eye(n) + diag(ones(n-1,1), -1);
A(1,:) = 1;
A

nnz(A)
sprintf('Just %d out of %d elements are non-zeros', nnz(A), n*n)
spy(A)

A_sparse = sparse(A);

whos A A_sparse


%% 1.2)
[L,U,P] = lu(A);



subplot(1,3,1)
title('A')
spy(A)

subplot(1,3,2)
title('L')
spy(L)

subplot(1,3,3)
title('U')
spy(U)
% We do observe the fill-in phenomenon


%% 1.3)
D_inv = diag(1./diag(A))
B_j = eye(n) - D_inv*A;

T = tril(A)
B_gs = eye(n) - inv(T)*A;

rho_B_j = max(abs(eig(B_j)))
rho_B_gs = max(abs(eig(B_gs)))


%% 1.4-1.5)
n = length(diag(A))
b = ones(n,1); b(1) = 2;
x0 = ones(n,1);
toll = 1e-6;
itermax = 1000;

[x,k] = jacobi(A, b, x0, toll, itermax)

norm(A*x-b)

%% 2;
n = 7
A = 9*eye(n) + -3*diag(ones(n-1,1),1) + -3*diag(ones(n-1,1),-1) + diag(ones(n-2,1),2) + diag(ones(n-2,1),-2)
b = [7 4 5 5 5 4 7]'


isDom = 1;

for i = 1:n
    
    a_ii = A(i,i);
    
    sum_rowAbs = sum(abs(A(i,:))) - abs(a_ii);
    
    if (abs(a_ii) < sum_rowAbs)
        
        isDom = 0;
        
    end
    
end


if isDom
    disp('SI dom. diag.')
else
    disp('NO dom. diag.')
end


v = eig(A);

if A == A'
    
    if (min(v)) > 0
        disp('Matrice SDP')

    else
        disp('Matrice NON SDP')
    end

else
    disp('Matrice NON SDP')
end




x0 = zeros(n,1);
tol = 1e-6;
itermax = 1000;

[x_j,k_j] = jacobi(A,b,x0,tol,itermax)
[x_gs,k_gs] = gs(A,b,x0,tol,itermax)










