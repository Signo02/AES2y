function T = es23(n)
T = zeros(n);
for ii = 1:n
    T(ii, ii:n) = 1:n-ii+1;
end
end
