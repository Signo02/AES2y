%% EX.1
close all

lambda = 2.4;
t_0 = 0; t_f = 3;
y_0 = 0.1;

f = @(t,y) lambda*y;

h = 0.05;
[t_h, u_h] = feuler(f,t_f,y_0,h);

plot(t_h, u_h)
grid on
hold on

h = 0.01;
[t_h, u_h] = feuler(f,t_f,y_0,h);
plot(t_h, u_h)


y_ex = @(t) y_0*exp(lambda*t);
plot(t_h, y_ex(t_h), 'LineWidth', 2)

legend('h_1=0.05','h_2=0.01', 'y_{ex}')

h_vect = 0.4.*0.5.^[0:4];
figure
e_h_vect = [];
for i = 1:length(h_vect)

    h = h_vect(i);
    [t_h, u_h] = feuler(f,t_f,y_0,h);
   
    e_h = max(abs(y_ex(t_h) - u_h));
    e_h_vect = [e_h_vect e_h];
end
loglog(h_vect, e_h_vect)
hold on
grid on
loglog(h_vect, h_vect)



%%
figure

subplot(1,2,1)

h = 0.05;
[t_h1, u_h, k_fp_vect1] = beuler(f,t_f,y_0,h);
plot(t_h1, u_h)
grid on
hold on


h = 0.01;
[t_h2, u_h, k_fp_vect2] = beuler(f,t_f,y_0,h);
plot(t_h2, u_h)

y_ex = @(t) y_0*exp(lambda*t);
plot(t_h2, y_ex(t_h2), 'LineWidth', 2)

legend('h_1=0.05','h_2=0.01', 'y_{ex}')


subplot(1,2,2)
plot(t_h1(2:end), k_fp_vect1)
hold on
grid on
plot(t_h2(2:end), k_fp_vect2)
legend('h_1=0.05','h_2=0.01')


h_vect = 0.4.*0.5.^[0:4];
figure
e_h_vect = [];
for i = 1:length(h_vect)

    h = h_vect(i);
    [t_h, u_h] = feuler(f,t_f,y_0,h);
   
    e_h = max(abs(y_ex(t_h) - u_h));
    e_h_vect = [e_h_vect e_h];
end
loglog(h_vect, e_h_vect)
hold on
grid on
loglog(h_vect, h_vect)





