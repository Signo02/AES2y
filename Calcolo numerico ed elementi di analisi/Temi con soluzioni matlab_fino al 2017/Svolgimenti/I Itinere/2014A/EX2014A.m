close all
%% 1.2
[A,b] = matrAndVect(5)
n = length(diag(A));

B_GS = eye(n)-inv(tril(A))*A;

%% 1.3
rho_B_GS = max(abs(eig(B_GS)))
% rho_B_GS = 0.1875 < 1 -> GS Converge


%% 1.5
x0 = b;
tol = 1e-6;
itermax = 100;
[ xk, k ] = gs( A,b,x0,tol,itermax )


%% 2.1
f = @(x) exp(x) - sin(x) - 1.5;
x = [-1:0.001:1];
df = @(x) exp(x)-cos(x);
plot(x,f(x))
grid on
hold on
plot (x,df(x))
legend('f(x)','f''(x)')
axis equal

%% 2.4
x0 = -0.3;
itermax = 100;
tol = 1e-4;

[x_vect,k] = newton(x0,itermax,tol,f,df);
xk = x_vect(end)
k


%% 2.5
alpha_exact = 0.794618530180279;
err = abs(xk-alpha_exact)

% m=1 -> Criterio diff. iterate successive soddisfacente

%% 2.6
stimap(x_vect)

% vediamo che ordine di conv. p=2 (infatti m=1)
